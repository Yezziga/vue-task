
let app = new Vue({
	el: '#exchange',
	data() { 
		return {
			data: {
				rates: []
			},
			exg: "SEK",
			selectedFromCurrency: "SEK",
			selectedToCurrency: "USD",
			inputAmount: 0,
			outputAmount: 0,
			country:{

			},
		}
	},
	created(){
		axios({url: 'https://api.exchangeratesapi.io/latest?base=SEK'})
		.then(response => {this.data=response.data; })
		axios({url: 'https://restcountries.eu/rest/v2/currency/sek'})
		.then(response => {
			this.country=response.data[0]; console.log(response.data[0].name);
		})
	},
	mounted() {
	
  	}, 
	  methods: {
		  /**
		   * Requests from API & updates the data-object depending on what currency is to be used
		   * @param {suffix to append with} url 
		   */
		updateData(url) {
			// in case given end of url would be undefined
			if(url==undefined) {
				url='latest?base=' + this.exg
			}
			axios({				
				url: 'https://api.exchangeratesapi.io/' + url
			})
			.then(response => {
				this.data=response.data
				// EUR is not included in the rates-array when requesting from API
				// & needs to be added manually.
				if(this.selectedFromCurrency=="EUR") {
					this.data.rates.EUR = 1
					// console.log(this.data.rates.EUR);
				}
			} )
			.then(() => {this.outputAmount = this.inputAmount*this.data.rates[this.selectedToCurrency]})
			.then(()=> {
				axios({url: 'https://restcountries.eu/rest/v2/currency/' + this.exg})
				.then(response => {
				this.country=response.data[0]; console.log(response.data[0].name);
			})
		})		
		},
		/**
		 * Forwards the clicked currency & changes the selecting option at the same time
		 * @param {currency that was clicked} currency 
		 */
		clickEvent(currency) {			
			this.selectedFromCurrency = currency
			this.onChange()			
		},
		/**
		 * Listens to changes in the input-field & changes the data accordingly.
		 * @param {amount of currency to convert} amount 
		 */
		onChange(amount) {
			// console.log(amount);
			// console.log(this.selectedFromCurrency);
			this.exg = this.selectedFromCurrency
			let url 
			if(this.selectedFromCurrency=="EUR") {
				url = 'latest'
			} else {
				url = 'latest?base=' + this.exg
			}
			this.updateData(url)	
			if(amount!=null) {
				this.inputAmount = amount	
			} 
		}		
  	}
})